<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_assets', function (Blueprint $table) {
            $table->id();
            $table->integer('job_id')->comment('relation to the job');
            $table->integer('job_service_id')->comment('relation to the job service');
            $table->string('name', 128)->nullable()->comment('asset name');
            $table->string('url', 256)->nullable()->comment('asset url');
            $table->string('extension', 8)->nullable()->comment('asset file extension');
            $table->integer('status')->default(1)->comment('1: general, 2: ready for delivery');
            $table->timestamps();
            $table->softDeletesTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_assets');
    }
}
