<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name', 32);
            $table->string('last_name', 32);
            $table->string('email')->unique();
            $table->integer('status')->default(3)->comment('1: active, 2: inactive, 3: invitation pending, 4: email verification pending');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('avatar_url', 225)->nullable();
            $table->string('gender', 1)->default('U');
            $table->json('role_ids')->comment('Json relationship to user_roles table');
            $table->string('guid', 32)->unique()->nullable();
            $table->json('meta_data')->nullable()->comment('Json object: {dob: string, job_title: string, is_internal: bool, color: string} - is_internal: internal or contractor, color: user preferred color');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletesTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
