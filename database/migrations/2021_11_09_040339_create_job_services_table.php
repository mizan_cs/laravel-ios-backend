<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_services', function (Blueprint $table) {
            $table->id();
            $table->integer('job_id')->comment('relation to the job');
            $table->integer('service_id')->comment('relation to the service');
            $table->string('delivery_preference', 500)->nullable();
            $table->json('assigned_user_ids')->nullable()->comment('relation to assigned users');
            $table->integer('status')->default(1)->comment('1: Shoot, 2: Edit, 3: Review, 4: revision, 5: deliver, 6: cancelled');
            $table->timestamps();
            $table->softDeletesTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_services');
    }
}
