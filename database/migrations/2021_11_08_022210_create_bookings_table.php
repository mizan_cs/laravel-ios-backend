<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->integer('client_id');
            $table->integer('package_id')->nullable()->comment('related to packages table');
            $table->json('job_location')->nullable();
            $table->string('job_requirements', 500)->nullable();
            $table->json('notify_contact_ids')->nullable()->comment('contact user ids that should get notifications about this booking');
            $table->integer('status')->default(1)->comment('1: pending, 2: accepted, 3: cancelled');
            $table->timestamps();
            $table->softDeletesTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
