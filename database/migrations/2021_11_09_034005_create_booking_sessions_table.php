<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_sessions', function (Blueprint $table) {
            $table->id();
            $table->integer('booking_id');
            $table->dateTime('session_date_time');
            $table->json('session_user_ids')->nullable();
            $table->integer('status')->default(1)->comment('1: booked, 2: completed, 3: cancelled');
            $table->string('comments')->comment('For shooters to add any comments about the session if required.');
            $table->timestamps();
            $table->softDeletesTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_sessions');
    }
}
