<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('name', 128);
            $table->integer('xero_account_id')->nullable();
            $table->integer('status')->default(1);
            $table->integer('primary_contact_id')->nullable()->comment('Primary contact user id');
            $table->json('contact_ids')->nullable()->comment('List of user ids connected to this client as contacts including primary contact id');
            $table->timestamps();
            $table->softDeletesTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
