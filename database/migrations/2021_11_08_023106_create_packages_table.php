<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            $table->string('name', 128);
            $table->string('description', 500)->nullable();
            $table->json('service_ids')->nullable()->nullable('Service ids related to this package');
            $table->json('sessions')->nullable()->comment('Json Object Array: {time: string, time_of_day: string}[]');
            $table->json('delivery_user_ids')->nullable()->comment('User ids of users who can deliver the package');
            $table->float('package_cost')->nullable();
            $table->timestamps();
            $table->softDeletesTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
