<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('name', 128);
            $table->string('type', 64);
            $table->string('group', 64);
            $table->string('description', 500)->nullable();
            $table->float('contractor_cost')->nullable();
            $table->float('upload_limit')->nullable();
            $table->float('max_number_of_files')->nullable();
            $table->json('shoot_length')->nullable()->comment('Length of the shoot in hours or number of required sessions.');
            $table->integer('status')->default(1)->comment('1: active, 2: archived');
            $table->timestamps();
            $table->softDeletesTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
