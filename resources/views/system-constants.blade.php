@extends('layouts.auth')

@section('styles')
    <link rel="stylesheet" href="{{ secure_asset('assets/css/style.css') }}" media="all">
    <script>
        hljs.initHighlightingOnLoad();
    </script>
@stop

@section('content')
    <div class="container-fluid">

        <?php
            $constants      = \App\Models\System\SystemConstant::get();
        ?>

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">System Constants</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
                            <li class="breadcrumb-item active">System Constants</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Constants list -->
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-striped mb-0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Key</th>
                            <th>Value</th>
                            <th>Is Secret</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($constants as $constant)
                            <form method="POST" action="/system-constants/save-key">
                                {!! csrf_field() !!}
                                <tr>
                                    <th scope="row">{{ $constant->id }}</th>
                                    <td>{{ $constant->key }}</td>
                                    <td>
                                        <input class="constant-key-input" type="{{ $constant->is_secret ? 'password' : 'text' }}" value="{{ $constant->value }}" name="value" />
                                    </td>
                                    <td>
                                        <input type="checkbox" value="1" {{ $constant->is_secret ? 'checked' : '' }} name="is_secret"/>
                                    </td>
                                    <td>
                                        <button class="btn btn-success">Save</button>
                                    </td>
                                    <input type="hidden" name="id" value="{{ $constant->id }}"/>
                                </tr>
                            </form>
                        @endforeach
                        </tbody>
                    </table>
                </div> <!-- end table-responsive-->
            </div>
        </div>

        <!-- Action buttons -->
        <div class="row">
            <div class="col-md-12 text-right mt-5">
                <form method="POST" action="/system-constants/deploy">
                    {!! csrf_field() !!}
                    <button class="btn btn-primary">Deploy Constants</button>
                </form>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <style>
        .constant-key-input {
            background: transparent;
            border: none;
            height: 100%;
            width: fit-content;
            min-width: 100px;
        }
    </style>
    <script src="js/script.js"></script>
@stop
