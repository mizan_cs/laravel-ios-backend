<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Information -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('/vendor/telescope/favicon.ico') }}">

    <meta name="robots" content="noindex, nofollow">

    <title>Admin :: ISO Bladescenes</title>

    <link rel="icon" href="favicon.ico">

    <!-- Style sheets-->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ asset(mix($cssFile, 'vendor/telescope')) }}" rel="stylesheet" type="text/css">

    <!-- App css -->
    <link href="{{ secure_asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ secure_asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ secure_asset('assets/css/theme.min.css') }}" rel="stylesheet" type="text/css" />
</head>
<body>

<!-- Begin page -->
<div id="telescope" v-cloak>

    <header id="page-topbar">
        <div class="navbar-header">

            <div class="d-flex align-items-left">
                <div class="d-flex align-items-center py-4 header">

                    <button class="btn btn-outline-primary ml-auto mr-3" v-on:click.prevent="toggleRecording" title="Play/Pause">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" class="icon fill-primary" v-if="recording">
                            <path d="M5 4h3v12H5V4zm7 0h3v12h-3V4z"/>
                        </svg>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" class="icon fill-primary" v-else>
                            <path d="M4 4l12 6-12 6z"/>
                        </svg>
                    </button>

                    <button class="btn btn-outline-primary mr-3" v-on:click.prevent="clearEntries" title="Clear Entries">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" class="icon fill-primary">
                            <path d="M6 2l2-2h4l2 2h4v2H2V2h4zM3 6h14l-1 14H4L3 6zm5 2v10h1V8H8zm3 0v10h1V8h-1z"/>
                        </svg>
                    </button>

                    <button class="btn btn-outline-primary mr-3" :class="{active: autoLoadsNewEntries}" v-on:click.prevent="autoLoadNewEntries" title="Auto Load Entries">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" class="icon fill-primary">
                            <path d="M10 3v2a5 5 0 0 0-3.54 8.54l-1.41 1.41A7 7 0 0 1 10 3zm4.95 2.05A7 7 0 0 1 10 17v-2a5 5 0 0 0 3.54-8.54l1.41-1.41zM10 20l-4-4 4-4v8zm0-12V0l4 4-4 4z"></path>
                        </svg>
                    </button>

                    <div class="btn-group" role="group" aria-label="Basic example">
                        <router-link tag="button" to="/monitored-tags" class="btn btn-outline-primary" active-class="active" title="Monitoring">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" class="icon fill-primary">
                                <path d="M12 10a2 2 0 0 1-3.41 1.41A2 2 0 0 1 10 8V0a9.97 9.97 0 0 1 10 10h-8zm7.9 1.41A10 10 0 1 1 8.59.1v2.03a8 8 0 1 0 9.29 9.29h2.02zm-4.07 0a6 6 0 1 1-7.25-7.25v2.1a3.99 3.99 0 0 0-1.4 6.57 4 4 0 0 0 6.56-1.42h2.1z"></path>
                            </svg>
                        </router-link>
                    </div>
                </div>
            </div>

            <div class="d-flex align-items-center">

                <div class="dropdown d-inline-block">
                    <button type="button" class="btn header-item noti-icon waves-effect"
                            id="page-header-notifications-dropdown" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        <i class="mdi mdi-bell"></i>
                        <span class="badge badge-danger badge-pill">3</span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0"
                         aria-labelledby="page-header-notifications-dropdown">
                        <div class="p-3">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h6 class="m-0"> Notifications </h6>
                                </div>
                                <div class="col-auto">
                                    <a href="#!" class="small"> View All</a>
                                </div>
                            </div>
                        </div>
                        <div data-simplebar style="max-height: 230px;">
                            <a href="" class="text-reset notification-item">
                                <div class="media">
                                    <img src="assets/images/users/avatar-2.jpg"
                                         class="mr-3 rounded-circle avatar-xs" alt="user-pic">
                                    <div class="media-body">
                                        <h6 class="mt-0 mb-1">Samuel Coverdale</h6>
                                        <p class="font-size-13 mb-1">You have new follower on Instagram</p>
                                        <p class="font-size-12 mb-0 text-muted"><i
                                                class="mdi mdi-clock-outline"></i> 2 min ago</p>
                                    </div>
                                </div>
                            </a>
                            <a href="" class="text-reset notification-item">
                                <div class="media">
                                    <div class="avatar-xs mr-3">
                                            <span class="avatar-title bg-success rounded-circle">
                                                <i class="mdi mdi-cloud-download-outline"></i>
                                            </span>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="mt-0 mb-1">Download Available !</h6>
                                        <p class="font-size-13 mb-1">Latest version of admin is now available.
                                            Please download here.</p>
                                        <p class="font-size-12 mb-0 text-muted"><i
                                                class="mdi mdi-clock-outline"></i> 4 hours ago</p>
                                    </div>
                                </div>
                            </a>
                            <a href="" class="text-reset notification-item">
                                <div class="media">
                                    <img src="assets/images/users/avatar-3.jpg"
                                         class="mr-3 rounded-circle avatar-xs" alt="user-pic">
                                    <div class="media-body">
                                        <h6 class="mt-0 mb-1">Victoria Mendis</h6>
                                        <p class="font-size-13 mb-1">Just upgraded to premium account.</p>
                                        <p class="font-size-12 mb-0 text-muted"><i
                                                class="mdi mdi-clock-outline"></i> 1 day ago</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="p-2 border-top">
                            <a class="btn btn-sm btn-light btn-block text-center" href="javascript:void(0)">
                                <i class="mdi mdi-arrow-down-circle mr-1"></i> Load More..
                            </a>
                        </div>
                    </div>
                </div>

                <div class="dropdown d-inline-block ml-2">
                    <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="rounded-circle header-profile-user" src="{{ secure_asset('assets/images/su-avatar.jpg') }}" alt="Header Avatar">
                        <span class="d-none d-sm-inline-block ml-1">
                            {{ \Illuminate\Support\Facades\Auth::user()->first_name }}
                            {{ \Illuminate\Support\Facades\Auth::user()->last_name }}
                        </span>
                        <i class="mdi mdi-chevron-down d-none d-sm-inline-block"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item d-flex align-items-center justify-content-between"
                           href="javascript:void(0)">
                            <span>Profile</span>
                            <span>
                                    <span class="badge badge-pill badge-warning">1</span>
                                </span>
                        </a>
                        <a class="dropdown-item d-flex align-items-center justify-content-between"
                           href="javascript:void(0)">
                            Settings
                        </a>
                        <a class="dropdown-item d-flex align-items-center justify-content-between" href="/auth/logout">
                            <span>Log Out</span>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </header>

    <!-- ========== Left Sidebar Start ========== -->
    <div class="vertical-menu">

        <div data-simplebar class="h-100">

            <div class="navbar-brand-box">
                <a href="/" class="logo">
                    <img src="{{ secure_asset('assets/images/dark-logo.png') }}" />
                </a>
            </div>

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <!-- Left Menu Start -->
                <ul class="metismenu list-unstyled" id="side-menu">
                    <li class="menu-title">Menu</li>
                    @include('layouts.nav')
                    <li>
                        <a href="javascript: void(0);" class="has-arrow waves-effect"><i class="bx bx-desktop"></i><span>System Monitor</span></a>
                        <ul class="sub-menu" aria-expanded="false">
                            <li class="nav-item">
                                <router-link active-class="active" to="/requests" class="nav-link d-flex align-items-center pt-0">
                                    <span>Requests</span>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link active-class="active" to="/commands" class="nav-link d-flex align-items-center">
                                    <span>Commands</span>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link active-class="active" to="/schedule" class="nav-link d-flex align-items-center">
                                    <span>Schedule</span>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link active-class="active" to="/jobs" class="nav-link d-flex align-items-center">
                                    <span>Jobs</span>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link active-class="active" to="/batches" class="nav-link d-flex align-items-center">
                                    <span>Batches</span>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link active-class="active" to="/cache" class="nav-link d-flex align-items-center">
                                    <span>Cache</span>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link active-class="active" to="/dumps" class="nav-link d-flex align-items-center">
                                    <span>Dumps</span>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link active-class="active" to="/events" class="nav-link d-flex align-items-center">
                                    <span>Events</span>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link active-class="active" to="/exceptions" class="nav-link d-flex align-items-center">
                                    <span>Exceptions</span>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link active-class="active" to="/gates" class="nav-link d-flex align-items-center">
                                    <span>Gates</span>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link active-class="active" to="/client-requests" class="nav-link d-flex align-items-center">
                                    <span>HTTP Client</span>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link active-class="active" to="/logs" class="nav-link d-flex align-items-center">
                                    <span>Logs</span>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link active-class="active" to="/mail" class="nav-link d-flex align-items-center">
                                    <span>Mail</span>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link active-class="active" to="/models" class="nav-link d-flex align-items-center">
                                    <span>Models</span>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link active-class="active" to="/notifications" class="nav-link d-flex align-items-center">
                                    <span>Notifications</span>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link active-class="active" to="/queries" class="nav-link d-flex align-items-center">
                                    <span>Queries</span>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link active-class="active" to="/views" class="nav-link d-flex align-items-center">
                                    <span>Views</span>
                                </router-link>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- Sidebar -->
        </div>
    </div>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">System Monitor</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
                                    <li class="breadcrumb-item active">System Monitor</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="col-md-12">
                        <div>
                            <alert :message="alert.message"
                                   :type="alert.type"
                                   :auto-close="alert.autoClose"
                                   :confirmation-proceed="alert.confirmationProceed"
                                   :confirmation-cancel="alert.confirmationCancel"
                                   v-if="alert.type"></alert>

                            <router-view></router-view>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page-content -->

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        {{ \Carbon\Carbon::now()->format('Y') }} © BLADESCENES
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-right d-none d-sm-block">
                            <a href="https://www.flume.agency/service/websites" target="_blank"> Design & Develop</a> by <a href="https://www.flume.agency/" target="_blank">Flume</a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

    </div>
    <!-- end main content-->

</div>
<!-- END layout-wrapper -->

<!-- Overlay-->
<div class="menu-overlay"></div>
<!-- Global Telescope Object -->
<script>
    window.Telescope = @json($telescopeScriptVariables);
</script>

<script src="{{ asset(mix('app.js', 'vendor/telescope')) }}"></script>
<script src="{{ secure_asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ secure_asset('assets/js/metismenu.min.js') }}"></script>
<script src="{{ secure_asset('assets/js/theme.js') }}"></script>
</body>
</html>
