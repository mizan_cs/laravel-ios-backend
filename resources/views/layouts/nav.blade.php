<li>
    <a href="/" class="waves-effect"><i class='bx bx-home-smile'></i><span class="badge badge-pill badge-primary float-right">7</span><span>Dashboard</span></a>
</li>
<li>
    <a href="javascript: void(0);" class="has-arrow waves-effect"><i class="bx bx-file"></i><span>Developer Documents</span></a>
    <ul class="sub-menu" aria-expanded="false">
        <li><a href="/developer-docs/local-setup">Local Setup</a></li>
        <li><a href="/developer-docs/models">Models</a></li>
        <li><a href="/developer-docs/system-constants">System Constants</a></li>
    </ul>
</li>
<li>
    <a href="/system-constants" class="waves-effect"><i class='bx bx-key'></i><span>System Constants</span></a>
</li>
