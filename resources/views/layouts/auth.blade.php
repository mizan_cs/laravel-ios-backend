<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex, nofollow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <title>Admin :: ISO Bladescenes</title>

    <!-- App css -->
    <link href="{{ secure_asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ secure_asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ secure_asset('assets/css/theme.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Syntax highlight -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/11.3.1/styles/default.min.css">

    @yield('styles')
</head>
<body>

<!-- Begin page -->
<div id="layout-wrapper">

    <header id="page-topbar">
        <div class="navbar-header">

            <div class="d-flex align-items-left">
                <!-- blank -->
            </div>

            <div class="d-flex align-items-center">

                <div class="dropdown d-inline-block">
                    <button type="button" class="btn header-item noti-icon waves-effect"
                            id="page-header-notifications-dropdown" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        <i class="mdi mdi-bell"></i>
                        <span class="badge badge-danger badge-pill">3</span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0"
                         aria-labelledby="page-header-notifications-dropdown">
                        <div class="p-3">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h6 class="m-0"> Notifications </h6>
                                </div>
                                <div class="col-auto">
                                    <a href="#!" class="small"> View All</a>
                                </div>
                            </div>
                        </div>
                        <div data-simplebar style="max-height: 230px;">
                            <a href="" class="text-reset notification-item">
                                <div class="media">
                                    <img src="assets/images/users/avatar-2.jpg"
                                         class="mr-3 rounded-circle avatar-xs" alt="user-pic">
                                    <div class="media-body">
                                        <h6 class="mt-0 mb-1">Samuel Coverdale</h6>
                                        <p class="font-size-13 mb-1">You have new follower on Instagram</p>
                                        <p class="font-size-12 mb-0 text-muted"><i
                                                class="mdi mdi-clock-outline"></i> 2 min ago</p>
                                    </div>
                                </div>
                            </a>
                            <a href="" class="text-reset notification-item">
                                <div class="media">
                                    <div class="avatar-xs mr-3">
                                            <span class="avatar-title bg-success rounded-circle">
                                                <i class="mdi mdi-cloud-download-outline"></i>
                                            </span>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="mt-0 mb-1">Download Available !</h6>
                                        <p class="font-size-13 mb-1">Latest version of admin is now available.
                                            Please download here.</p>
                                        <p class="font-size-12 mb-0 text-muted"><i
                                                class="mdi mdi-clock-outline"></i> 4 hours ago</p>
                                    </div>
                                </div>
                            </a>
                            <a href="" class="text-reset notification-item">
                                <div class="media">
                                    <img src="assets/images/users/avatar-3.jpg"
                                         class="mr-3 rounded-circle avatar-xs" alt="user-pic">
                                    <div class="media-body">
                                        <h6 class="mt-0 mb-1">Victoria Mendis</h6>
                                        <p class="font-size-13 mb-1">Just upgraded to premium account.</p>
                                        <p class="font-size-12 mb-0 text-muted"><i
                                                class="mdi mdi-clock-outline"></i> 1 day ago</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="p-2 border-top">
                            <a class="btn btn-sm btn-light btn-block text-center" href="javascript:void(0)">
                                <i class="mdi mdi-arrow-down-circle mr-1"></i> Load More..
                            </a>
                        </div>
                    </div>
                </div>

                <div class="dropdown d-inline-block ml-2">
                    <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="rounded-circle header-profile-user" src="{{ secure_asset('assets/images/su-avatar.jpg') }}" alt="Header Avatar">
                        <span class="d-none d-sm-inline-block ml-1">
                            {{ \Illuminate\Support\Facades\Auth::user()->first_name }}
                            {{ \Illuminate\Support\Facades\Auth::user()->last_name }}
                        </span>
                        <i class="mdi mdi-chevron-down d-none d-sm-inline-block"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item d-flex align-items-center justify-content-between"
                           href="javascript:void(0)">
                            <span>Profile</span>
                            <span>
                                    <span class="badge badge-pill badge-warning">1</span>
                                </span>
                        </a>
                        <a class="dropdown-item d-flex align-items-center justify-content-between"
                           href="javascript:void(0)">
                            Settings
                        </a>
                        <a class="dropdown-item d-flex align-items-center justify-content-between" href="/auth/logout">
                            <span>Log Out</span>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </header>

    <!-- ========== Left Sidebar Start ========== -->
    <div class="vertical-menu">

        <div data-simplebar class="h-100">

            <div class="navbar-brand-box">
                <a href="/" class="logo">
                    <img src="{{ secure_asset('assets/images/dark-logo.png') }}" />
                </a>
            </div>

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <!-- Left Menu Start -->
                <ul class="metismenu list-unstyled" id="side-menu">
                    <li class="menu-title">Menu</li>
                    @include('layouts.nav')
                    <li>
                        <a href="javascript: void(0);" class="has-arrow waves-effect"><i class="bx bx-desktop"></i><span>System Monitor</span></a>
                        <ul class="sub-menu" aria-expanded="false">
                            <li><a href="/system-monitoring/requests">Requests</a></li>
                            <li><a href="/system-monitoring/commands">Commands</a></li>
                            <li><a href="/system-monitoring/schedule">Schedule</a></li>
                            <li><a href="/system-monitoring/jobs">Jobs</a></li>
                            <li><a href="/system-monitoring/batches">Batches</a></li>
                            <li><a href="/system-monitoring/cache">Cache</a></li>
                            <li><a href="/system-monitoring/dumps">Dumps</a></li>
                            <li><a href="/system-monitoring/events">Events</a></li>
                            <li><a href="/system-monitoring/exceptions">Exceptions</a></li>
                            <li><a href="/system-monitoring/gates">Gates</a></li>
                            <li><a href="/system-monitoring/client-requests">HTTP Client</a></li>
                            <li><a href="/system-monitoring/logs">Logs</a></li>
                            <li><a href="/system-monitoring/mail">Mail</a></li>
                            <li><a href="/system-monitoring/models">Models</a></li>
                            <li><a href="/system-monitoring/notifications">Notifications</a></li>
                            <li><a href="/system-monitoring/queries">Queries</a></li>
                            <li><a href="/system-monitoring/views">Views</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- Sidebar -->
        </div>
    </div>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">

            <!-- Page content -->
            @yield('content')
            <!-- End page content -->

        </div>
        <!-- End Page-content -->

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        {{ \Carbon\Carbon::now()->format('Y') }} © BLADESCENES
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-right d-none d-sm-block">
                            <a href="https://www.flume.agency/service/websites" target="_blank"> Design & Develop</a> by <a href="https://www.flume.agency/" target="_blank">Flume</a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

    </div>
    <!-- end main content-->

</div>
<!-- END layout-wrapper -->

<!-- Overlay-->
<div class="menu-overlay"></div>


<!-- jQuery  -->
<script src="{{ secure_asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ secure_asset('assets/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ secure_asset('assets/js/metismenu.min.js') }}"></script>
<script src="{{ secure_asset('assets/js/waves.js') }}"></script>
<script src="{{ secure_asset('assets/js/simplebar.min.js') }}"></script>

<!-- Morris Js-->
<script src="{{ secure_asset('assets/plugins/morris-js/morris.min.js') }}"></script>
<!-- Raphael Js-->
<script src="{{ secure_asset('assets/plugins/raphael/raphael.min.js') }}"></script>

<!-- Morris Custom Js-->
<script src="{{ secure_asset('assets/pages/dashboard-demo.js') }}"></script>

<!-- App js -->
<script src="{{ secure_asset('assets/js/theme.js') }}"></script>

<!-- Syntax highlight -->
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/11.3.1/highlight.min.js"></script>
<script>hljs.highlightAll();</script>

@yield('scripts')

</body>
</html>
