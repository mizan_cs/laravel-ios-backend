<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <title>Admin :: Bladescenes</title>

    <!-- App css -->
    <link href="{{ secure_asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ secure_asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ secure_asset('assets/css/theme.min.css') }}" rel="stylesheet" type="text/css" />
</head>
<body>

<div>
    <!-- container -->
    <div class="container">
        <div class="row">
            <div class="col-12">

                <!-- Content -->
                @yield('content')
                <!-- End Content -->

            </div>
        </div>
    </div>
    <!-- end container -->
</div>

<!-- App js -->
<script src="{{ secure_asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ secure_asset('assets/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ secure_asset('assets/js/metismenu.min.js') }}"></script>
<script src="{{ secure_asset('assets/js/waves.js') }}"></script>
<script src="{{ secure_asset('assets/js/simplebar.min.js') }}"></script>
<script src="{{ secure_asset('assets/js/theme.js') }}"></script>

</body>
</html>
