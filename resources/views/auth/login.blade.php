@extends('layouts.guest')

@section('content')
    <!-- Login page -->
    <div class="d-flex align-items-center min-vh-100">
        <div class="w-100 d-block my-5">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-5">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-center mb-4 mt-3">
                                <a href="index.html">
                                    <span><img src="{{ secure_asset('assets/images/logo.png') }}" alt="" height="26"></span>
                                </a>
                            </div>
                            <form method="POST" action="/auth/login">
                                {!! csrf_field() !!}
                                @if ($errors->any())
                                    @foreach ($errors->all() as $error)
                                        <span class="has-error">{{$error}}</span>
                                    @endforeach
                                @endif
                                <div class="form-group">
                                    <label for="emailaddress">Email address</label>
                                    <input class="form-control" placeholder="Type your email..." type="email" name="email" value="{{ old('email') }}" required tabindex="1">
                                </div>
                                <div class="form-group">
                                    <a href="pages-recoverpw.html" class="text-muted float-right">Forgot your password?</a>
                                    <label for="password">Password</label>
                                    <input class="form-control" placeholder="Type your password..." type="password" name="password" id="password" required tabindex="2">
                                </div>

                                <div class="form-group mb-4 pb-3">
                                    <div class="custom-control custom-checkbox checkbox-primary">
                                        <input type="checkbox" class="custom-control-input" name="remember">
                                        <label class="custom-control-label" for="checkbox-signin">Remember me</label>
                                    </div>
                                </div>
                                <div class="mb-3 text-center">
                                    <button class="btn btn-primary btn-block" type="submit" tabindex="3"> Login </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Login page -->
@stop
