@extends('layouts.auth')

@section('styles')
    <link rel="stylesheet" href="{{ secure_asset('assets/css/style.css') }}" media="all">
    <script>
        hljs.initHighlightingOnLoad();
    </script>
@stop

@section('content')
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Developer Documents</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
                            <li class="breadcrumb-item active">System Constants</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-md-12">
                <div class="type-definition-content-page">
                    <div class="type-definition-content">
                        <div class="overflow-hidden content-section" id="content-get-started">
                            <h1>System Constants</h1>
                            <p>
                                Any constant value inside the system can be defined under the system constant. System constants are living in the database system_constants table. The main usage of the system constants are to store keys and other type of constant variable that can change easily using the system admin dashboard.
                            </p>
                            <h2>How it works</h2>
                            <p>
                                System constants managing UI is located here under root system constants nav item. Inside the nav item, there is a Deploy Constants button. That button will check the defined system constant keys and deploy any of the keys currencly not exists in the database. It will only deploy the key name and value needs to be added manually. Also, by clicking this button will not override any existing keys or values. It will only deploy the new keys that is not currently exists in the databse.
                            </p>
                            <h2>Technical Info</h2>
                            <p>
                                Deploy Constant function will check the constants defined inside the app/Services/System/Constants/ConstantService.php -> $requiredConstants variable.
                            </p>
                            <h2>How to add a system constant</h2>
                            <ul>
                                <li>
                                    Go to app/Services/System/Constants/ConstantService.php
                                </li>
                                <li>
                                    Add your constant value to $requiredConstants array.
                                </li>
                                <li>
                                    Open the super admin UI -> <a href="/system-constants" target="_blank">System Constants</a> click Deploy Constants.
                                </li>
                                <li>
                                    After the app deploys, you will be able to see your constant name (key), now you can add the key value and save it.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <style>
        .github-corner:hover .octo-arm {
            animation: octocat-wave 560ms ease-in-out
        }

        @keyframes octocat-wave {
            0%,
            100% {
                transform: rotate(0)
            }
            20%,
            60% {
                transform: rotate(-25deg)
            }
            40%,
            80% {
                transform: rotate(10deg)
            }
        }

        @media (max-width:500px) {
            .github-corner:hover .octo-arm {
                animation: none
            }
            .github-corner .octo-arm {
                animation: octocat-wave 560ms ease-in-out
            }
        }
        @media only screen and (max-width:680px){ .github-corner > svg { right: auto!important; left: 0!important; transform: rotate(270deg)!important;}}
    </style>
    <script src="js/script.js"></script>
@stop
