@extends('layouts.auth')

@section('styles')
    <link rel="stylesheet" href="{{ secure_asset('assets/css/style.css') }}" media="all">
    <script>
        hljs.initHighlightingOnLoad();
    </script>
@stop

@section('content')
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Developer Documents</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
                            <li class="breadcrumb-item active">Local Setup</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-md-12">
                <div class="type-definition-content-page">
                    <div class="type-definition-content">
                        <div class="overflow-hidden content-section" id="content-get-started">
                            <h1>Models</h1>
                            <p>
                                Models available in the system has defined below.
                            </p>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                @if($modelsData !== null && $modelsData->code === 200 && is_array($modelsData->data))
                                <div id="accordion" class="custom-accordion mb-4">
                                    @foreach($modelsData->data as $key => $model)
                                    <div class="card mb-0">
                                        <div class="card-header" id="model-{{$key}}">
                                            <h5 class="m-0 font-size-15">
                                                <a class="d-block pt-2 pb-2 text-dark" data-toggle="collapse" href="#collapse-{{$key}}" aria-expanded="true" aria-controls="collapseOne">
                                                    {{ $model['name'] }} <span class="float-right"><i class="mdi mdi-chevron-down accordion-arrow"></i></span>
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-{{$key}}" class="collapse" aria-labelledby="model-{{$key}}" data-parent="#accordion">
                                            <div class="card-body">
                                                <div class="overflow-hidden content-section" id="content-get-characters">
                                                    <h4>Definition</h4>
                                                    <p>{{ $model['description'] }}</p>
                                                    <table>
                                                        <thead>
                                                        <tr>
                                                            <th>Field</th>
                                                            <th>Type</th>
                                                            <th>Length</th>
                                                            <th>Default</th>
                                                            <th>Is Unique</th>
                                                            <th>Is Nullable</th>
                                                            <th>Description</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($model['columns'] as $column)
                                                        <tr>
                                                            <td>{{ $column['name'] }}</td>
                                                            <td>{{ $column['type'] }}</td>
                                                            <td>{{ $column['length'] === 0 ? '-' : $column['length'] }}</td>
                                                            <td>{{ $column['default'] }}</td>
                                                            <td>{{ $column['is_unique'] ? 'TRUE' : 'FALSE' }}</td>
                                                            <td>{{ $column['is_nullable'] ? 'TRUE' : 'FALSE' }}</td>
                                                            <td>{{ $column['comment'] }}</td>
                                                        </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- end card-->
                                    @endforeach
                                </div> <!-- end custom accordions-->
                                @else
                                <p>No models found!</p>
                                @endif
                            </div> <!-- end col -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <style>
        .github-corner:hover .octo-arm {
            animation: octocat-wave 560ms ease-in-out
        }

        @keyframes octocat-wave {
            0%,
            100% {
                transform: rotate(0)
            }
            20%,
            60% {
                transform: rotate(-25deg)
            }
            40%,
            80% {
                transform: rotate(10deg)
            }
        }

        @media (max-width:500px) {
            .github-corner:hover .octo-arm {
                animation: none
            }
            .github-corner .octo-arm {
                animation: octocat-wave 560ms ease-in-out
            }
        }
        @media only screen and (max-width:680px){ .github-corner > svg { right: auto!important; left: 0!important; transform: rotate(270deg)!important;}}
    </style>
    <script src="js/script.js"></script>
@stop
