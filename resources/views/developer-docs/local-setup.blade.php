@extends('layouts.auth')

@section('content')
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Developer Documents</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
                            <li class="breadcrumb-item active">Local Setup</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center">Local Setup</h1>
                <p class="text-center">Instruction to setup Bladescenes ISO in your local.</p>
            </div>
            <div class="col-md-12">
                <h2>ISO Base</h2>
                <p>
                    ISO Base is the main API and business login control application. This is built on top of Laravel 8 framework with PHP 8.0 and and MySQL 8.0. Laravel Sail has used to do the local development in the application.
                </p>

                <h4>Pre-requirements</h4>
                <ul>
                    <li>Docker v4.0 or higher</li>
                </ul>

                <h4>Local Setup</h4>
                <ol>
                    <li>Clone the iso-base repository to your local computer.</li>
                    <li>Make sure your docker is running.</li>
                    <li>Contact one of the previous developers and get the local .env file and store that in iso-base root path.</li>
                    <li>
                        Open your computer host file using your preferred editor (Mac Eg: sudo nano /etc/hosts) and add the following line.<br/>
                        <code class="language-bash">127.0.0.1 iso-base.local</code><br/>
                        If you want to change the local domain, you will have to generate a new key pair under project-root/proxy/certs.
                    </li>
                    <li>
                        After all the above setup run the following.<br/>
                        <code class="language-bash">$ docker-compose up</code>
                    </li>
                    <li>Once the docker is up and running, visit <a href="https://iso-base.local" target="_blank">https://iso-base.local</a> and you should be able to see the login page.
                    <li>As the final setup, check the .env file database credentials and remote login to the database and import the database.</li>
                </ol>
            </div>
            <div class="col-md-12 mt-5">
                <h2>ISO UI</h2>
                <p>
                    ISO UI is the main web interface for Bladescenes ISO web portal. This is built on top of NuxtJs 2.15.
                </p>

                <h4>Pre-requirements</h4>
                <ul>
                    <li>Node v14.0 or higher</li>
                </ul>
                <h4>Local Setup</h4>
                <ol>
                    <li>Clone the iso-ui repository to your local computer.</li>
                    <li>Contact one of the previous developers and get the local .env file and store that in iso-ui root path.</li>
                    <li>
                        install dependencies.<br/>
                        <code class="language-bash">$ npm run dev</code>
                    </li>
                    <li>
                        Create the local certificate.<br/>
                        <code class="language-bash">
                            $ cd .certificates<br/>
                            $ mkcert iso-ui.local
                        </code>
                    </li>
                    <li>
                        Open your computer host file using your preferred editor (Mac Eg: sudo nano /etc/hosts) and add the following line.<br/>
                        <code class="language-bash">127.0.0.1 iso-ui.local</code>
                    </li>
                    <li>
                        serve with hot reload at https://iso-ui.local.<br/>
                        <code class="language-bash">$ npm run dev</code>
                    </li>
                    <li>Once the docker is up and running, visit <a href="https://iso-ui.local" target="_blank">https://iso-ui.local</a> and you should be able to see the login page.</li>
                    <li>ISO UI is fully depend on ISO Base. Therefore you need to make sure ISO Base is up and running on https://iso-base.local.</li>
                </ol>
            </div>
        </div>
    </div>
@stop
