<?php

namespace App\Services\System\Constants;

use App\Models\System\SystemConstant;
use App\Services\Service;

class ConstantService extends Service  {

    private static $requiredConstants = [
        ['key' => 'GOOGLE_API_KEY', 'is_secret' => true],
        ['key' => 'GOOGLE_API_SERVICE', 'is_secret' => true],
        ['key' => 'XERO_API_KEY', 'is_secret' => true],
        ['key' => 'XERO_API_SECRET', 'is_secret' => true],
    ];

    /**
     * Deploy the defined system constants. This will not override any existing
     *
     * @return object
     */
    public static function deployConstants () {
        try {
            $systemConstants            = SystemConstant::pluck('key')->toArray();

            foreach (self::$requiredConstants as $requiredConstant) {
                // If the provided requiredConstants array doesn't have a key or if the value already exists in the database, skipping to next
                if (!isset($requiredConstant['key']) || in_array($requiredConstant['key'], $systemConstants)) {
                    continue;
                }

                // If not exists in the db and has a correct key attribute, then create a new record
                $constant               = new SystemConstant();
                $constant -> key        = $requiredConstant['key'];
                $constant -> is_secret  = !(isset($requiredConstant['is_secret']) && $requiredConstant['is_secret'] === false);
                $constant -> save();
            }

            return self::returnData(true);
        } catch (\Exception $ex) {
            report($ex);
            return self::returnData($ex->getMessage(), 500, $ex);
        }
    }
}
