<?php

namespace App\Services\System\Models;

use App\Services\Service;

class ModelDefinitionExtractService extends Service  {
    /**
     * Generate the definitions
     */
    public static function generateDefinitions () {
        try {
            // Array to hold final return data
            $returnData         = array();

            // Get migration files
            $migrationFiles     = glob(database_path('migrations') . '/*.php');

            // Loop the file and extract the required
            foreach ($migrationFiles as $migrationFile) {
                $fileContent    = file_get_contents($migrationFile);
                $isTelescope    = self::get_string_between($fileContent, '$this->schema->create(\'', '\'');
                if ($isTelescope === 'telescope_entries' ) {
                    continue;
                }
                $name           = self::get_string_between($fileContent, 'Schema::create(\'', '\', function (Blueprint $table)');
                $description    = self::get_string_between($fileContent, '%desc:');
                $records        = self::get_string_array_between($fileContent, '$table-', ';');

                $columns        = array();
                if (is_array($records)) {
                    foreach ($records as $record) {
                        // Setup the primary key data
                        if ($record === '>id()') {
                            $columns        []= array(
                                'name'          => 'id',
                                'type'          => 'primary_key',
                                'length'        => '-',
                                'default'       => '-',
                                'comment'       => self::get_string_between($record, '->comment(\'', '\')'),
                                'is_unique'     => true,
                                'is_nullable'   => false,
                            );
                        } else {
                            // Setup other column data
                            $length             = self::get_string_between($record, '\',', ')');
                            $default            = self::get_string_between($record, '->default(', ')');
                            $isUnique           = self::get_string_between($record, '->uniqu', '()');
                            $isNullable         = self::get_string_between($record, '->nullabl', '()');

                            $columns        []= array(
                                'name'          => self::get_string_between($record, '\'', '\''),
                                'type'          => self::get_string_between($record, '>', '('),
                                'length'        => $length === "" && is_numeric($length) ? '-' : (int)$length,
                                'default'       => $default === "" ? '-' : $default,
                                'comment'       => self::get_string_between($record, '->comment(\'', '\')'),
                                'is_unique'     => $isUnique !== "",
                                'is_nullable'   => $isNullable !== "",
                            );
                        }
                    }
                }

                $returnData   []= array(
                    'name'          => $name,
                    'description'   => $description,
                    'columns'       => $columns,
                );
            }

            return Service::returnData($returnData);

        } catch (\Exception $ex) {
            report($ex);
            return Service::returnData($ex->getMessage(), 500, $ex);
        }
    }

    /**
     * Get substring between start and end substrings
     *
     * @param $string
     * @param $start
     * @param $end
     * @return string
     */
    private static function get_string_between($string, $start, $end = null) {
        $pos_string = stripos($string, $start);

        if ($pos_string === false) {
            return "";
        }

        $substr_data = substr($string, $pos_string);
        $string_two = substr($substr_data, strlen($start));

        if ($end === null) {
            $second_pos = strpos($string_two, PHP_EOL);
        } else {
            $second_pos = stripos($string_two, $end);
        }

        $string_three = substr($string_two, 0, $second_pos);
        $result_unit = trim($string_three);
        return $result_unit;
    }

    /**
     * Get substring between start and end substrings
     *
     * @param $string
     * @param $start
     * @param $end
     * @return string | array
     */
    private static function get_string_array_between($string, $start, $end = null) {
        $strippedString     = $string;
        $foundStringsArray  = array();

        do {
            $foundString        = self::get_string_between($strippedString, $start, $end);

            if ($foundString !== "") {
                $foundStringsArray  []= $foundString;
            }

            $strippedString     = str_replace($start.$foundString, '', $strippedString);

        } while($foundString !== "");

        return $foundStringsArray;
    }
}
