<?php

namespace App\Services\GoogleCalendar;

use App\Services\Service;

abstract class CalendarCore extends Service  {

    protected $calendarScope;
    protected $calendarAccessType;
    protected $calendarConsent;
    private $client;
    private $service;

    /**
     * Constructor
     *
     * @throws \Google\Exception
     */
    public function __construct() {
        // Set configs
        $applicationName                    = env('GOOGLE_CALENDAR_APPLICATION_NAME');
        $authConfig                         = env('GOOGLE_CALENDAR_AUTH_CONFIG_PATH');
        $calendarScope                      = $this->getCalendarScope() ?? \Google_Service_Calendar::CALENDAR_EVENTS;
        $calendarAccessType                 = $this->getCalendarAccessType() ?? 'offline';
        $calendarConsent                    = $this->getCalendarConsent() ?? null;

        // Initiate google calendar client
        $this->client                             = new \Google_Client();
        $this->client->setApplicationName($applicationName);
        $this->client->setScopes($calendarScope);
        $this->client->setAuthConfig($authConfig);
        $this->client->setAccessType($calendarAccessType);
        $this->client->setPrompt($calendarConsent);

        // Initiate calendar service
        $this->service                      = new \Google_Service_Calendar($this->client);
    }


    // Core functions
    // -----------------------------------------------------------------------------------------------------------------
    /**
     * Create a new event
     *
     * @return object|void
     */
    protected function coreCreateEvent (object $eventData) {
        try {

            // Create a new event from the given event data
            $newEvent           = $this->service->events($eventData);

            // Throw the error if the event creation status is not successful
            if ($newEvent->status !== 'success') {
                throw new \Exception($eventData->error ?? 'Unknown!', 500);
            }

            // Return the event data
            return Service::returnData($newEvent->data);

        } catch (\Exception $ex) {
            report($ex);
            return Service::returnData($ex->getMessage(), 500, $ex);
        }
    }

    /**
     * Edit an event
     *
     * @return object|void
     */
    protected function coreEditEvent (int $eventId, object $eventData) {
        try {

            // Create a new event from the given event data
            $newEvent           = $this->service->editEvent($eventId, $eventData);

            // Throw the error if the event creation status is not successful
            if ($newEvent->status !== 'success') {
                throw new \Exception($eventData->error ?? 'Unknown!', 500);
            }

            // Return the event data
            return Service::returnData($newEvent->data);

        } catch (\Exception $ex) {
            report($ex);
            return Service::returnData($ex->getMessage(), 500, $ex);
        }
    }

    /**
     * Check permission by email
     *
     * @return object|void
     */
    protected function coreCheckPermissionByEmail (string $emailAddress) {
        try {

            //

        } catch (\Exception $ex) {
            report($ex);
            return Service::returnData($ex->getMessage(), 500, $ex);
        }
    }

    /**
     * Send access request by email
     *
     * @return object|void
     */
    protected function coreSendAccessRequestByEmail (string $emailAddress) {
        try {

            //

        } catch (\Exception $ex) {
            report($ex);
            return Service::returnData($ex->getMessage(), 500, $ex);
        }
    }

    /**
     * Invite a user by email
     *
     * @return object|void
     */
    protected function coreInviteAUserByEmail (int $eventId, string $emailAddress) {
        try {

            //

        } catch (\Exception $ex) {
            report($ex);
            return Service::returnData($ex->getMessage(), 500, $ex);
        }
    }

    /**
     * Delete event
     *
     * @return object|void
     */
    protected function coreDeleteEvent (int $eventId) {
        try {

            //

        } catch (\Exception $ex) {
            report($ex);
            return Service::returnData($ex->getMessage(), 500, $ex);
        }
    }


    // Config getters and setters
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Get calendar consent
     * @return mixed
     */
    public function getCalendarConsent() {
        return $this->calendarConsent;
    }

    /**
     * Set calendar consent
     * @param mixed $calendarConsent
     */
    public function setCalendarConsent($calendarConsent): void {
        $this->calendarConsent = $calendarConsent;
    }

    /**
     * Get calendar access type
     * @return mixed
     */
    public function getCalendarAccessType() {
        return $this->calendarAccessType;
    }

    /**
     * Set calendar access type
     * @param mixed $calendarAccessType
     */
    public function setCalendarAccessType($calendarAccessType): void {
        $this->calendarAccessType = $calendarAccessType;
    }

    /**
     * Get calendar scope
     * @return mixed
     */
    public function getCalendarScope() {
        return $this->calendarScope;
    }

    /**
     * Set calendar scope
     * @param mixed $calendarScope
     */
    public function setCalendarScope($calendarScope): void {
        $this->calendarScope = $calendarScope;
    }
}
