<?php

namespace App\Services\GoogleCalendar;


class CalendarService extends CalendarCore  {

    /**
     * Create a new calendar event in the master calendar
     */
    public static function createEvent (object $eventData) {
        return (new self)->coreCreateEvent($eventData);
    }

    /**
     * Edit an existing event by event id
     *
     * @param int $eventId
     * @param object $eventData
     */
    public static function editEvent (int $eventId, object $eventData) {
        return (new self)->coreEditEvent($eventId, $eventData);
    }

    /**
     * Check the permission status of the given email address
     *
     * @param string $emailAddress
     */
    public static function checkPermissionByEmail (string $emailAddress) {
        return (new self)->coreCheckPermissionByEmail($emailAddress);
    }

    /**
     * Send access request to the given email address
     *
     * @param string $emailAddress
     */
    public static function sendAccessRequestByEmail (string $emailAddress) {
        return (new self)->coreSendAccessRequestByEmail($emailAddress);
    }

    /**
     * Send an event invite to the given email address
     *
     * @param int $eventId
     * @param string $emailAddress
     */
    public static function inviteAUserToEventByEmail (int $eventId, string $emailAddress) {
        return (new self)->coreInviteAUserByEmail($eventId, $emailAddress);
    }

    /**
     * Delete the given calendar event
     *
     * @param int $eventId
     */
    public static function deleteEvent (int $eventId) {
        return (new self)->coreDeleteEvent($eventId);
    }
}
