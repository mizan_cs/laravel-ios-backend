<?php

namespace App\Services;

class Service {
    /**
     * Return service data
     *
     * @param $data
     * @param int $code
     * @param null $error_msg
     * @return object
     */
    public static function returnData ($data, int $code = 200, \Exception $ex = null) {
        return (object) [
            'data'          => $data,
            'code'          => $code,
            'ex'            => $ex,
        ];
    }
}
