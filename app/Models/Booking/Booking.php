<?php

namespace App\Models\Booking;

use App\Models\Client\Client;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Staudenmeir\EloquentJsonRelations\HasJsonRelationships;

class Booking extends Model
{
    use HasFactory, SoftDeletes, HasJsonRelationships;

    protected $casts = [
        'job_location'          => 'json',
        'notify_contact_ids'    => 'json',
    ];

    /**
     * @return \Staudenmeir\EloquentJsonRelations\Relations\BelongsToJson
     */
    public function contacts () {
        return $this->belongsToJson(User::class, 'notify_contact_ids');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client () {
        return $this->belongsTo(Client::class, 'client_id');
    }
}
