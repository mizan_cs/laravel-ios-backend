<?php

namespace App\Models\Client;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Staudenmeir\EloquentJsonRelations\HasJsonRelationships;

class Client extends Model
{
    use HasFactory, SoftDeletes, HasJsonRelationships;

    protected $casts = [
        'contact_ids'    => 'json',
    ];

    /**
     * @return \Staudenmeir\EloquentJsonRelations\Relations\BelongsToJson
     */
    public function contacts () {
        return $this->belongsToJson(User::class, 'contact_ids');
    }
}
