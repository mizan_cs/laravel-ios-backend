<?php

namespace App\Models\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Staudenmeir\EloquentJsonRelations\HasJsonRelationships;

class UserRole extends Model
{
    use HasFactory, SoftDeletes, HasJsonRelationships;

    protected $casts = [
        'access'        => 'json',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users () {
        return $this->hasManyJson(User::class, 'role_ids');
    }
}
