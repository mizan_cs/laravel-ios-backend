<?php

namespace App\Models;

use App\Models\Client\Client;
use App\Models\Contact\PhoneNumber;
use App\Models\User\UserRole;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use \Staudenmeir\EloquentJsonRelations\HasJsonRelationships;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasJsonRelationships;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'role_ids'          => 'json',
        'meta_data'         => 'json',
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return \Staudenmeir\EloquentJsonRelations\Relations\BelongsToJson
     */
    public function roles () {
        return $this->belongsToJson(UserRole::class, 'role_ids');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function phoneNumbers () {
        return $this->belongsToMany(PhoneNumber::class, 'user_phones', 'phone_id', 'user_id');
    }

    /**
     * @return \Staudenmeir\EloquentJsonRelations\Relations\HasManyJson
     */
    public function clients () {
        return $this->hasManyJson(Client::class, 'contact_ids');
    }
}
