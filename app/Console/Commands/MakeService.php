<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class MakeService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:service {className}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new service class';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(FileSystem $files)
    {
        parent::__construct();
        $this->files = $files;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        $name = trim($this->argument('className'));
        $base_path  = 'Services/'.substr($name, 0,strrpos($name, '/'));

        if (! file_exists(app_path('Services'))) {
            mkdir(app_path('Services'), 0777);
            $service_class_content = $this->files->get(__DIR__ . '/Stubs/service.stub');
            $this->files->put(app_path('Services/Service.php'), $service_class_content);
        }


        if (! file_exists(app_path($base_path)))
            mkdir(app_path($base_path), 0777, true);

        if (! $name or is_null($name) or empty($name)) {
            $this->error('Not enough arguments (missing: "className").');
            return false;
        }

        $this->createService($name);
    }

    /**
     * Create a new service
     *
     * @param $name
     * @return bool
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    private function createService($name) {
        $name = $this->checkServiceName($name);
        $base_path  = 'App/Services/'.substr($name, 0,strrpos($name, '/'));
        $file = "App/Services/".$name . '.php';

        if ($this->files->exists(app_path('Services/' . $file))) {
            $this->error('Service already exists!');
            return false;
        }

        $original = $this->files->get(__DIR__ . '/Stubs/services.stub');

        $original = str_replace('App\Services;', ucfirst(str_replace('/', '\\', $base_path)).';', $original);
        $original = str_replace('ServiceName', ucfirst(basename($name)), $original);
        $this->files->put(app_path('Services/' . $name . '.php'), $original);

        $this->info('Service created successfully.');
        return true;
    }

    /**
     * Check service name and format accordingly
     *
     * @param $name
     * @return string
     */
    private function checkServiceName($name) {
        if (strpos($name, 'Service'))
            return ucfirst($name);
        else if (strpos($name, 'service'))
            return ucfirst(str_replace('service', 'Service', $name));
        else
            return ucfirst($name) . 'Service';
    }
}
