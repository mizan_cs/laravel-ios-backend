<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct() {
        // Get language
        $lang   = request()->header('locale', 'en');
        App::setLocale($lang);
    }

    /**
     * Wrap the response data
     *
     * @param $result
     * @param int $code
     * @param \Exception|null $ex
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendResponse ($result, $code = 200, \Exception $ex = null) {
        $response       = [
            'success'       => $code === 200,
            'code'          => $code,
            'data'          => $result
        ];

        if (in_array(env('APP_ENV'), ['local', 'dev', 'development']) && $code !== 200) {
            $response   = array_merge($response, [
                'error'     => $ex !== null ? $ex->getMessage() : null,
                'trace'     => $ex !== null ? $ex->getTrace() : null,
            ]);
        }

        if ($code === 500 && $result === null) {
          $response['data'] = 'Oops! Something went wrong. Please try again later';
        }

        return response()->json($response, in_array($code, [200, 201, 406, 403]) ? 200 : $code);
    }
}
