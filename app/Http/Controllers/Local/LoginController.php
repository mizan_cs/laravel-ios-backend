<?php

namespace App\Http\Controllers\Local;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class LoginController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get login view
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function getLogin () {
        return View::make('auth.login');
    }

    /**
     * Post Login
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postLogin (Request $request) {
        try {
            $request->validate([
                'email'         => 'required',
                'password'      => 'required'
            ]);

            $credentials = $request->except(['_token']);

            // Check user role to access su (Super user) admin section
            $user = User::where('email', $request->email)->first();
            if ($user === null || !in_array(500, $user->role_ids)) {
                session()->flash('message', 'Invalid credentials');
                return redirect()->back()->withErrors(['Username or password is invalid!']);
            }

            // Check the credentials
            if (auth()->attempt($credentials)) {
                return redirect('/');
            }else{
                session()->flash('message', 'Invalid credentials');
                return redirect()->back()->withErrors(['Username or password is invalid!']);
            }

        } catch (\Exception $ex) {
            report($ex);
            return response($ex->getMessage());
        }
    }

    /**
     * Logout
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout () {
        Auth::logout();
        return redirect()->route('su-login');
    }
}
