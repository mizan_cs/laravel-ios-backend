<?php

namespace App\Http\Controllers\Local;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class DashboardController extends Controller
{
    /**
     * Get Dashboard view
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function getDashboard () {
        return View::make('dashboard');
    }
}
