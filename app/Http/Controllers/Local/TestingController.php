<?php

namespace App\Http\Controllers\Local;

use App\Http\Controllers\Controller;
use App\Models\Client\Client;
use App\Models\User;
use App\Services\System\Constants\ConstantService;
use App\Services\System\Models\ModelDefinitionExtractService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class TestingController extends Controller
{
    public function getTest () {
        if (env('APP_ENV' !== 'local')) abort(404);
        ConstantService::deployConstants();
//        $user = User::first();
//        auth()->login($user, true);
//
        //return redirect()->route('su-dashboard');
//        $user = new User();
//        $user->first_name = 'Mizan';
//        $user->last_name = 'R';
//        $user->email = 'mizan@mizan.com';
//        $user->email_verified_at = Carbon::now()->toDateTimeString();
//        $user->password = Hash::make('password');
//        $user->role_ids = [700];
//        $user->save();
        return 'test';
    }
}
