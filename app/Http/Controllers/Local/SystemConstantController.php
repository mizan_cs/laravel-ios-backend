<?php

namespace App\Http\Controllers\Local;

use App\Http\Controllers\Controller;
use App\Models\System\SystemConstant;
use App\Services\System\Constants\ConstantService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class SystemConstantController extends Controller
{
    /**
     * Get system constants
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\JsonResponse
     */
    public function getSystemConstants () {
        try {

            return View::make('system-constants');

        } catch (\Exception $ex) {
            report($ex);
            return $this->sendResponse($ex->getMessage(), 500, $ex);
        }
    }

    /**
     * Deploy any new system constants
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\JsonResponse
     */
    public function deploySystemConstants () {
        try {

            // Deploying any new constants
            $constantsDeployed      = ConstantService::deployConstants();

            return $constantsDeployed->data
                ? View::make('system-constants')
                : $this->sendResponse($constantsDeployed->data, $constantsDeployed->code, $constantsDeployed->ex);

        } catch (\Exception $ex) {
            report($ex);
            return $this->sendResponse($ex->getMessage(), 500, $ex);
        }
    }

    /**
     * Save the given system constant
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\JsonResponse
     */
    public function saveSystemConstant (Request $request) {
        try {
            // Get the request data
            $constantId                         = $request->get('id');
            $constantValue                      = $request->get('value');
            $isSecret                           = $request->get('is_secret');

            // Get the relevant constant by the given value
            $systemConstant                     = SystemConstant::where('id', $constantId)->first();

            // Save the value and is_secret attribute if the constant exists under the given id
            if ($systemConstant !== null) {
                $systemConstant -> value        = $constantValue;
                $systemConstant -> is_secret    = $isSecret === '1';
                $systemConstant -> save();
            }

            // Return back to the view
            return View::make('system-constants');

        } catch (\Exception $ex) {
            report($ex);
            return $this->sendResponse($ex->getMessage(), 500, $ex);
        }
    }
}
