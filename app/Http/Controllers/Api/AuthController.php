<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\Service;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /**
     * Login user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return object|void
     */
    public function login (Request $request) {
        try {

            // Validate the request
            $validator = Validator::make($request->all(), [
                'email'         => 'required|string|max:255',
                'password'      => 'required|string|min:6',
            ]);

            // Return error response if validation is false
            if ($validator->fails()) {
                return Service::returnData(['errors'=>$validator->errors()->all()], 422);
            }

            // Get user with requested email
            $user               = User::where('email', $request->email)->first();

            if ($user) {
                if (Hash::check($request->password, $user->password)) {

                    //TODO make the fromDeviceName dynamic
                    $fromDeviceName = 'Desktop';
                    $token = $user->createToken($fromDeviceName)->plainTextToken;

                    return Service::returnData(['token'=>$token], 200);
                } else {
                    $response   = ["message" => "Password mismatch"];
                    return Service::returnData($response, 422);
                }
            } else {
                $response       = ["message" =>'Invalid login credentials'];
                return Service::returnData($response, 422);
            }

        } catch (\Exception $ex) {
            report($ex);
            return Service::returnData($ex->getMessage(), 500, $ex);
        }

    }

    /**
     * Logout user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return object|void
     */
    public function logout (Request $request) {
        // Get current user token
        $token                 = $request->user()->token();

        //Revoke the token
        $token->revoke();
        $response = ['message' => 'You have been successfully logged out!'];

        //Return the response
        return response($response, 200);
    }

    /**
     * Change user password
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request) {
        $validator = Validator::make($request->all(), [
            'password'          => 'required|confirmed|min:6',
            'current_password'  => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>'error','message'=>'Invalid form data']);
        }

        $user = User::find($request->get('user_id'));
        if ($user){
            $old_password       = $request->get('current_password');
            $new_password       = $request->get('password');
            if (Hash::check($old_password, $user->password)) {
                $user->password = bcrypt($new_password);
                $user->save();
                //Auth::login($user, true);
                return response()->json([
                    'status'    =>'error',
                    'message'   =>'Password updated successfully'
                ]);
            }else{
                return response()->json([
                    'status'    =>'error',
                    'message'   =>'Invalid old password'
                ]);
            }
        } else {
            return response()->json([
                'status'        =>'error',
                'message'       =>'Invalid user'
            ]);
        }

    }

    public function authUser(Request $request) {
        return Service::returnData($request->user(), 200);
    }

}

