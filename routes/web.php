<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Local\LoginController;
use App\Http\Controllers\Local\DashboardController;
use App\Http\Controllers\Local\TestingController;
use \App\Services\System\Models\ModelDefinitionExtractService;
use \App\Http\Controllers\Local\SystemConstantController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Local Web Authenticated Routes
 */
Route::group(['middleware' => 'auth'], function () {
    Route::get('/', [DashboardController::class, 'getDashboard'])->name('su-dashboard');

    // Developer docs routes
    Route::group(['prefix' => 'developer-docs'], function () {
        Route::get('/local-setup', function () {return view('developer-docs.local-setup');});
        Route::get('/models', function () {
            return view('developer-docs.models')->with(['modelsData' => ModelDefinitionExtractService::generateDefinitions()]);
        });
        Route::get('/system-constants', function () {return view('developer-docs.system-constants');});
    });

    // System constants
    Route::group(['prefix' => 'system-constants'], function () {
        Route::get('/', [SystemConstantController::class, 'getSystemConstants'])->name('su-system-constants');
        Route::post('/deploy', [SystemConstantController::class, 'deploySystemConstants'])->name('su-system-constants-deploy');
        Route::post('/save-key', [SystemConstantController::class, 'saveSystemConstant'])->name('su-system-constants-save');
    });

    // Logout route
    Route::get('/auth/logout', [LoginController::class, 'logout'])->name('su-logout');
});

/**
 * Functionality testing route
 */
Route::get('/testing', [TestingController::class, 'getTest'])->name('su-testing');

/**
 * Local Web Guest Routes
 */
Route::get('/auth/login', [LoginController::class, 'getLogin'])->name('su-login');
Route::post('/auth/login', [LoginController::class, 'postLogin'])->name('su-post-login');
