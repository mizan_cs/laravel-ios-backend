<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Passport Api 0Auth 2.0 Route*/
Route::post('/auth/login', [AuthController::class,'login'])->name('api.user-login');
Route::post('/password/change', [AuthController::class,'changePassword'])->name('api.user-password-change');

Route::group(['middleware' => 'auth:sanctum','prefix' => 'auth'], function ($router) {
    Route::post('/logout', [AuthController::class,'logout'])->name('api.user-logout');
    Route::post('/user', [AuthController::class,'authUser'])->name('api.auth-user');
});

/*Passport Api 0Auth 2.0 Route END*/

